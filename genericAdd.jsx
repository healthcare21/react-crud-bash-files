import React, { Component } from 'react'
import { connect } from 'react-redux'
import firebase from '../../../../firebase.js';
import {setMessage} from "../../../../actions/globalActions"
import {store} from "../../../../store"
import {formData} from "../../../../config/config"

//form utility calls
import {buildcurrentData} from '../buildform.utils'

 class AddForm extends Component {
    constructor(props) {
		super(props);
		this.state = {
            singleDist : '',
            id: '',
            collection: 'disciplines'
        }

        formData[0][this.state.collection].map((value) => {
            this.state[value.title] = '';
        })

         this.handleChange = this.handleChange.bind(this);
         this.updateDiscipline = this.updateDiscipline.bind(this);
    }

    updateDiscipline(event){
        event.preventDefault();
        let collection = this.state.collection;
        let discipline = firebase.firestore().collection(collection);
        let states = {};
        formData[0][this.state.collection].map((value) => {
           states[value.title] = this.state[value.title];
        })
        discipline.add(states)
        store.dispatch(setMessage({message: 'Add new..'}));
    }

    handleChange(event) {
        // console.log(event.target.value)
        let name = event.target.name;
        this.setState({ [name]: event.target.value});
    }

    render() {
       
       let formDataRet=  buildcurrentData(this.state.collection, this.handleChange, this.state, this.props);
        return(
            <>
            <h2>Name d</h2>
            <form onSubmit={this.updateDiscipline}>
            {formDataRet.map(function(item, i){
                    return( item )
                    })}
						<button type="submit"  id="update">Add Discipline</button>
						{/* <button type="button" class="btn btn-danger" id="delete">Delete</button> */}
				
				</form>
            </>
        )
    }
}

    const mapStateToProps = state => ({
        // disciplines: state.discipline
    })

    export default connect(mapStateToProps)(AddForm)
        