import React, { Component } from 'react'
import { connect } from 'react-redux'
import firebase from '../../../../firebase.js';
import {setMessage} from "../../../../actions/globalActions"
import {store} from "../../../../store"
import {formData} from "../../../../config/config"
import {buildcurrentData} from '../buildform.utils'
 class DisciplinesUpdate extends Component {
    constructor(props) {
		super(props);
		this.state = {
            stateDesc : '',
            id: '',
            collection: 'disciplines'
        }
        formData[0][this.state.collection].map((value) => {
            this.state[value.title] = '';
        })
         this.handleChange = this.handleChange.bind(this);
         this.updateDiscipline = this.updateDiscipline.bind(this);
    }



    updateDiscipline(event){
        event.preventDefault();
        let collection = this.state.collection;
        let discipline = firebase.firestore().collection(collection).doc(this.state.id);
        //combine all states
        let states = {};
        formData[0][this.state.collection].map((value) => {
           states[value.title] = this.state[value.title];
        })
        discipline.set(states)
        store.dispatch(setMessage({message: 'Updated..'}));
    }

    deleteItem(id){
        let discipline = firebase.firestore().collection([this.state.collection]).doc(this.state.id);
        discipline.delete().then(function(){
            store.dispatch(setMessage({message: 'Deleted..'}));
        })
    }

    handleChange(event) {
        let name = event.target.name;
        this.setState({ [name]: event.target.value});
    }
    
    componentDidUpdate() {
         if(this.props.stateDesc != this.state.stateDesc){
                if(this.props.stateDesc){
                    Object.keys(this.props.stateDesc).map((value) => {
                    this.setState({ [value]: this.props.stateDesc[value]});
        
                })
                }
            this.setState({  stateDesc: this.props.stateDesc })
        }      
      }

    render() {
        let formDataRet=  buildcurrentData(this.state.collection, this.handleChange, this.state, this.props);
        return(
            <>
            <h2>Name</h2>
            <form onSubmit={this.updateDiscipline}>
                    {formDataRet.map(function(item, i){
                    return( item )
                    })}
					<button type="submit"  id="update">Update</button>
					<button type="button"  onClick={() => this.deleteItem(this.state.id)}>Delete</button>
				</form>
            </>
        )
    }
}

    const mapStateToProps = state => ({
        
    })

    export default connect(mapStateToProps)(DisciplinesUpdate)
        