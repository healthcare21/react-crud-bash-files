import React, { Component } from 'react'
import { filter } from 'lodash';
import { connect } from 'react-redux'
import  DisciplinesUpdate  from './disciplinesUpdate'
import AddForm from './disciplinesAdd'
import {Message} from '../../../../components/common'

 class DisciplinesAdd extends Component {
    constructor(props) {
		super(props);
		this.state = {
            currentDiscipline : '',
            add : 'add',
            collection: 'disciplines'
        }
    }
 

    toggleAdd(){
        let addVar = this.state.add;
        if (addVar === ''){ this.setState({add : 'add'})} else { this.setState({add : ''})}
    }

    setDiscipline(ID){
        if(this.state.add === 'add'){
            this.toggleAdd();
        }
        const collection = this.props[this.state.collection];
        var unique = filter(collection[this.state.collection], function(o) { return o.id == ID; });
        this.setState({currentDiscipline : unique[0]});
    }

    render() {
        const hide = { display: 'none' }
        let updateAdd; let button; let addForm;
        if(this.state.add != 'add'){
            updateAdd = <div style={hide}><AddForm {...this.props}/> </div>
            addForm = <div><DisciplinesUpdate {...this.props} stateDesc = {this.state.currentDiscipline} /></div>
            button = <p  onClick={() => this.toggleAdd()}>Add new discipline</p>
        } else {
            updateAdd = <div><AddForm {...this.props} /></div>
            addForm = <div style={hide}><DisciplinesUpdate {...this.props} stateDesc = {this.state.currentDiscipline} /></div>
    
        }

        const collection = this.props[this.state.collection];
        return(
            <>
            <Message message = {this.props.message} messageType= {this.state.messageType} />
            {/* List all items */}
            <ul>
            {collection[this.state.collection].map((value, index) => {
                 return <li key={index}  onClick={() => this.setDiscipline(value.id)} >{value.title}</li>
            })}
            </ul>
                {button}
                {updateAdd}
                {addForm}
            </>
        )
    }
}

    const mapStateToProps = state => ({
        user: state.user,
        disciplines: state.discipline,
        message: state.global.message
    })

    export default connect(mapStateToProps)(DisciplinesAdd)
        